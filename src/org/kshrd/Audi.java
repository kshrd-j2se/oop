package org.kshrd;


// IS-A relationship
public class Audi extends Car {
    //Audi is a Car
    String color = "black";

    // Has-A relationship
    Engine engine = new Engine();

    void method() {
        engine.printEngine();
        printColor();
    }

}
