package org.kshrd.exercise1;

// Student IS-A Person
public class Student extends Person {

    public double math, khmer, chemistry, english, biology;

    // Student HAS-A book
    Book book = new Book();


    public Student(int id, String name, String gender, String dateOfBirth,double math, double khmer, double chemistry, double english, double biology) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.math = math;
        this.khmer = khmer;
        this.chemistry = chemistry;
        this.english = english;
        this.biology = biology;
    }


    public double findAverage() {
        return (math + khmer + chemistry + english + biology) / 5;
    }



    @Override
    public String toString() {
        return "Student{" +
                "math=" + math +
                "\n khmer=" + khmer +
                "\n chemistry=" + chemistry +
                "\n english=" + english +
                "\n biology=" + biology +
                "\n id=" + id +
                "\n name='" + name + '\'' +
                "\n gender='" + gender + '\'' +
                "\n dateOfBirth='" + dateOfBirth + '\'' +
                '}';
    }
}


